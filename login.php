<?php
header('content-type: application/json; charset=utf-8');

$email = $_POST['email'];
$pass  = $_POST['password'];

$users = array(
    array('email' => 'demo@treasury.gov.my', 'password' => 'password'),
    array('email' => 'test@treasury.gov.my', 'password' => 'password'),
    array('email' => 'hello@treasury.gov.my', 'password' => 'password'),
    array('email' => 'web@treasury.gov.my', 'password' => 'password'),
    array('email' => 'imof@treasury.gov.my', 'password' => 'password'),
);

$valid = false;


// LDAP Auth
foreach ($users as $user) {
    if ($email == $user['email'] && $pass == $user['password']) {
        $valid = true;
    }
}
// End LDAP Auth


if ($valid) {
    $details = json_encode(
        array(
            'status' => 1,
            'message' => 'Login success!'
        )
    );
}
else {
    $details = json_encode(
        array(
            'status' => 0,
            'message' => 'Login failed!'
        )
    );
}

if (isset($_REQUEST['callback'])) {
    echo $_REQUEST['callback'] . '('.$details.')';
}
else {
    echo $details;
}
exit;
