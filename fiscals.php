<?php
header('content-type: application/json; charset=utf-8');

$reports = array(
    array(
        'id' => '07-2015',
        'file' => 'http://mof.demosite.my/demo/pdf/weu_07_2015.pdf',
        'title' => 'Issue 7/2015 V4',
    ),
    array(
        'id' => '06-2015',
        'file' => 'http://mof.demosite.my/demo/pdf/weu_06_2015.pdf',
        'title' => 'Issue 6/2015 V4',
    ),
    array(
        'id' => '05-2015',
        'file' => 'http://mof.demosite.my/demo/pdf/weu_05_2015.pdf',
        'title' => 'Issue 5/2015 V4',
    ),
);


if (count($reports)) {
    $details = json_encode(
        array(
            'status' => 1,
            'reports' => $reports
        )
    );
}
else {
    $details = json_encode(
        array(
            'status' => 0,
        )
    );
}


if (isset($_REQUEST['callback'])) {
    echo $_REQUEST['callback'] . '('.$details.')';
}
else {
    echo $details;
}
exit;