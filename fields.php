<?php
header('content-type: application/json; charset=utf-8');

$fields = array(
    [
        'id' => 1,
        'code' => 'PB',
        'title' => 'Pengurusan Belanjawan'
    ],
    [
        'id' => 2,
        'code' => 'PK',
        'title' => 'Perolehan Kerajaan'
    ],
    [
        'id' => 3,
        'code' => 'WP',
        'title' => 'Pengurusan Wang Awam'
    ],
    [
        'id' => 4,
        'code' => 'KP',
        'title' => 'Pengurusan Aset'
    ],
    [
        'id' => 5,
        'code' => 'PS',
        'title' => 'Tadbir Urus Kewangan'
    ],
    [
        'id' => 6,
        'code' => 'PA',
        'title' => 'Pelaburan Strategik'
    ],
    [
        'id' => 7,
        'code' => 'PR',
        'title' => 'Pinjaman Perumahan'
    ],
);


if (count($fields)) {
    $details = json_encode(
        array(
            'status' => 1,
            'items' => $fields
        )
    );
}
else {
    $details = json_encode(
        array(
            'status' => 0,
        )
    );
}


if (isset($_REQUEST['callback'])) {
    echo $_REQUEST['callback'] . '('.$details.')';
}
else {
    echo $details;
}
exit;